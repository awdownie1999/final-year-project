import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import TrendingView from '../views/TrendingView.vue'
import ConvertView from '../views/ConvertView.vue'
import UserView from '../views/UserView.vue'

// setup of routes for the application
const routes = [
  {
    path: '/:access_token?/:not_converted?',
    name: 'home',
    component: HomeView
  },
  {
    path: '/trending',
    name: 'trending',
    component: TrendingView
  },
  {
    path: '/convert:music_service?:access_token?:playlist?',
    name: 'convert',
    component: ConvertView
  },
  {
    path: '/user',
    name: 'user',
    component: UserView
  }

]

//creation of the vue router
const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
