// imports for the Vue app
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import VueAgile from 'vue-agile'
import VueGapi from 'vue-gapi'
import { createAuth0 } from '@auth0/auth0-vue';
import Paginate from "vuejs-paginate-next";

// creation of vue app & modules that will be used
createApp(App)
.use(router)
.use(VueAgile)
.use(Paginate)
.use(VueGapi, {
    apiKey: 'AIzaSyDSscKWibTUAJuMC9Il_mCWfYOYAbtYgVc',
    clientId: '137004339037-ateh2fdqlir82eo8fgkhcf4jsis7d6m4.apps.googleusercontent.com',
    scope: 'https://www.googleapis.com/auth/youtube',
})
.use(
  createAuth0({
    domain: "dev-qqs0hz-c.us.auth0.com",
    client_id: "iBftuqh1Mw6dSVeSlchDh1ZtzXJBoQH9",
    redirect_uri: window.location.origin,
    audience: "localhost:8080",
  })
)
  .mount('#app')
  // mounting of vue app to App.vue