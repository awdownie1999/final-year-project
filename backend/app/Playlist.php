<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function track() {
        return $this->hasMany('App\Track');
    }

    protected $fillable = [
        'name', 'user_id', 'converted_from'
    ];

}
