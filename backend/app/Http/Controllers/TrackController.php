<?php

namespace App\Http\Controllers;

use App\Track;
use Illuminate\Http\Request;
use Log;

class TrackController extends Controller
{


    public function addTrack(Request $request)
    {
        Log::info($request);

        $track = Track::firstOrNew([
            'name' => $request->name,
            'playlist_id' => $request->playlist_id,
            'artist' => $request->artist,
        ]);

        $track->save();
        
        return response()->json('Track Added', 200);
    }


    public function getTracks(Request $request)
    {
        Log::info($request);

        $tracks = Track::where('playlist_id', $request->playlist_id)->get();
        Log::info($tracks);

        return response()->json($tracks, 200);
    }
    
}
