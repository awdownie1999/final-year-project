<?php

namespace App\Http\Controllers;

use App\Playlist;
use Illuminate\Http\Request;
use Log;

class PlaylistController extends Controller
{

    public function addPlaylist(Request $request)
    {
        Log::info($request);

        $playlist = Playlist::firstOrNew([
            'name' => $request->name,
            'user_id' => $request->user_id,
            'converted_from' => $request->converted_from
        ]);

        $playlist->save();
        
        return response()->json('Playlist Added', 200);
    }

    public function getPlaylists(Request $request)
    {
        Log::info($request);

        $playlists = Playlist::where('user_id', $request->user_id)->get();
        Log::info($playlists);

        return response()->json($playlists, 200);

    }


}
