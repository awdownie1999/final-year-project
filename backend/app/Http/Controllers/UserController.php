<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Log;


class UserController extends Controller
{

    public function addUser(Request $request)
    {
        Log::info($request);
        Log::info($request->name);

        $user = User::firstOrNew([
            'name' => $request->name,
            'auth0_id' => $request->auth0_id,
        ]);

        $user->save();
        
        return response()->json('User Added', 200);
    }

    public function getUser(Request $request)
    {
        Log::info($request);

        $users = User::all();

        foreach($users as $user){
            if($user->auth0_id == $request->auth0_id){
                return response()->json($user, 200);
            }
        }
        $data = ['message' => 'No user!'];
        return response()->json($data, 204);
    }

}
