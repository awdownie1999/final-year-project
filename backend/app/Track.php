<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Track extends Model
{
    protected $fillable = [
        'name', 'artist', 'playlist_id'
    ];

    public function playlist() {
        return $this->belongsTo('App\Playlist');
    }
}
