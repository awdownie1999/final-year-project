<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/addUser', 'UserController@addUser')->name('addUser');
Route::get('/getUser', 'UserController@getUser')->name('getUser');

Route::post('/addPlaylist', 'PlaylistController@addPlaylist')->name('addPlaylist');
Route::get('/getPlaylists', 'PlaylistController@getPlaylists')->name('getPlaylists');

Route::post('/addTrack', 'TrackController@addTrack')->name('addTrack');
Route::get('/getTracks', 'TrackController@getTracks')->name('getTracks');